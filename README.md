# Architecture & Principles:

Architecture: follow Clean Architecture

Principles: SOLID

# Third parties & libraries:
- Network: Okhttp and retrofit
- Thread handler: Rxjava, RxAndroid
- SQLite for caching: RoomDB
- MVVM pattern for Presentation & App layer using: ViewModel and Livedata in Android Architecture Components
- Dependency inversion principle: Dagger2
- Testing:
    + Unittest: mockito, powermock, junit
    + UI Test: expresso

# Folder structure

#### Application:
- App: presents the `app layer` and `presentation layer`.The layers using MVVM as a structure
- Domain: presents the `domain layer` which handles the business logic of the application in usecases. The usecases can have repositories interface implemented in the data layer.
- Data: presents `data layer`. The layer is responsible for fetching the data including local source, remote source (rest API).
- Shared: contains const variables, extensions, util functions for the whole application.

#### Testing:
***Unit Test:***

    + Mapper: test model mapping functions
    + Repository: test reppositories
    + Usecase: test usecases
    + ViewModel: test viewmodels
    
***UI Test:***

    + Activity: makes the script of features of the activities

# Steps in order to get the application run on local compute
- Clone source from the repository.
- Make sync to get dependences, libraries
- Clean the project then making the build.

# Checklist of items has done
1. Programming language: Kotlin
2. Design app's architecture: MMVM
3. Apply LiveData mechanism.
4. UI should be looks like in attachment.
5. Write UnitTests: Unit Test and UI Test
6. Acceptance Tests
7. Exception handling: handle network error, invalid input, http exceptions...
8. Caching handling: using RoomDB to cached for local data
9. Secure Android app from: 
    + Proguard to prevent decompile: add `weather_keystore.jks` as a keystore file for signing. it should not be committed to the repository because of security purpose in reality. However, this is just an assignment so I did it for convenience.
10. Accessibility for Disability Supports:
    a. Talkback: set ```android:contentDescription``` to the view to do "select to speak" actions.
    b. Scaling Text: Display size and font-size: using sp unit for textview to handle scaling text.
12. Readme file includes

