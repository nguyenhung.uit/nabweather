package uit.nguyenhung.nabweather.shared

const val FORECAST_DOMAIN_TO_UI_NAME = "FORECAST_DOMAIN_TO_UI_NAME"
const val FORECAST_RESPONSE_TO_UI_DOMAIN = "FORECAST_DOMAIN_TO_UI_NAME"

interface ModelMapper<From, To> {

    fun map(from: From): To
}