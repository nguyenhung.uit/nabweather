package uit.nguyenhung.nabweather.shared

fun Long?.safe(defaultValue: Long = 0) = this ?: defaultValue

fun Float?.safe(defaultValue: Float = 0f) = this ?: defaultValue

fun String?.safe(defaultValue: String = ""): String = this ?: defaultValue

fun Double?.safe(defaultValue: Double = 0.0) = this ?: defaultValue
