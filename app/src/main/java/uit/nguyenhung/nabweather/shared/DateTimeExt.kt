package uit.nguyenhung.nabweather.shared

import java.text.SimpleDateFormat
import java.util.*

var DEFAULT_PATTERN = "EEE',' dd MMM yyyy"

fun Date.format(pattern: String = DEFAULT_PATTERN, locale: Locale = Locale.getDefault()): String {
    val simpleDateFormat = SimpleDateFormat(pattern, locale)
    return simpleDateFormat.format(this)
}

fun Date.atMidnight(): Long {
    return Calendar.getInstance().apply {
        time = this@atMidnight
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.timeInMillis
}