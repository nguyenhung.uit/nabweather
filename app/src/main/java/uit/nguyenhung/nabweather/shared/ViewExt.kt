package uit.nguyenhung.nabweather.shared

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

fun EditText?.hideKeyboard() {
    this?.let { safeEdt ->
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(safeEdt.windowToken, 0)
    }
}