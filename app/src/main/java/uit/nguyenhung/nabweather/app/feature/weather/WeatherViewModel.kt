package uit.nguyenhung.nabweather.app.feature.weather

import androidx.annotation.CallSuper
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import uit.nguyenhung.nabweather.app.Event
import uit.nguyenhung.nabweather.app.mapper.ForecastDomainToUI
import uit.nguyenhung.nabweather.app.model.forecast.ForecastUI
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import uit.nguyenhung.nabweather.domain.usecase.CityQueryInvalidInput
import uit.nguyenhung.nabweather.domain.usecase.IGetWeatherForecastUseCase
import uit.nguyenhung.nabweather.domain.usecase.NoConnectionException
import uit.nguyenhung.nabweather.domain.usecase.WeatherHttpException
import uit.nguyenhung.nabweather.shared.safe

interface IWeatherViewModel {
    val forecastUI: LiveData<List<ForecastUI>>
    val invalidSearchInput: LiveData<Event<String>>
    val commonError: LiveData<Event<String>>

    fun getForecasts(
        cityQuery: String,
        forecastDayNumbers: Int,
        units: TemperatureUnit = TemperatureUnit.CELSIUS
    )
}

class WeatherViewModel @ViewModelInject constructor(
    private val getWeatherForecastUseCase: IGetWeatherForecastUseCase,
    private val forecastDomainToUI: ForecastDomainToUI,
) : ViewModel(), IWeatherViewModel {

    private val mDisposablesOnDestroy: CompositeDisposable = CompositeDisposable()

    private fun Disposable.addToDisposables() {
        this.addTo(mDisposablesOnDestroy)
    }

    private val _forecastUI = MutableLiveData<List<ForecastUI>>()
    override val forecastUI: LiveData<List<ForecastUI>>
        get() = _forecastUI

    private val _invalidSearchInput = MutableLiveData<Event<String>>()
    override val invalidSearchInput: LiveData<Event<String>>
        get() = _invalidSearchInput

    private val _commonError = MutableLiveData<Event<String>>()
    override val commonError: LiveData<Event<String>>
        get() = _commonError

    override fun getForecasts(
        cityQuery: String,
        forecastDayNumbers: Int,
        units: TemperatureUnit
    ) {
        getWeatherForecastUseCase.execute(cityQuery, forecastDayNumbers, units)
            .map {
                it.map { forecast ->
                    forecastDomainToUI.map(forecast)
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _forecastUI.value = it
            }, {
                when (it) {
                    is CityQueryInvalidInput -> {
                        _invalidSearchInput.value = Event(it.message.safe())
                    }
                    is WeatherHttpException,
                    is NoConnectionException -> {
                        _forecastUI.value = emptyList()
                        _commonError.value = Event(it.message.safe())
                    }
                    else -> {
                        _forecastUI.value = emptyList()
                        _commonError.value = Event("Something went wrong")
                    }
                }
            }).addToDisposables()
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        mDisposablesOnDestroy.dispose()
    }
}