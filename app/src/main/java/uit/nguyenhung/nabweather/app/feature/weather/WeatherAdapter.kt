package uit.nguyenhung.nabweather.app.feature.weather

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import uit.nguyenhung.nabweather.R
import uit.nguyenhung.nabweather.app.model.forecast.ForecastUI
import uit.nguyenhung.nabweather.databinding.ItemWeatherBinding

class WeatherAdapter(
    recyclerView: RecyclerView
) : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    init {
        recyclerView.run {
            adapter = this@WeatherAdapter
            addItemDecoration(DividerItemDecoration(context))
        }
    }

    val data: MutableList<ForecastUI> = mutableListOf()

    fun submitList(newList: List<ForecastUI>) {
        val diffCallback = WeatherDiffCallback(data, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        return WeatherViewHolder(parent)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount() = data.size

    inner class WeatherViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflateViewHolder(R.layout.item_weather)
    ) {

        private val viewBinding = ItemWeatherBinding.bind(itemView)

        @SuppressLint("SetTextI18n")
        fun onBind(item: ForecastUI) {
            viewBinding.run {
                tvDate.text = "Date: ${item.date}"
                tvAvgTemperature.text = "Average Temperature: ${item.avgTemperature}"
                tvPressure.text = "Pressure: ${item.pressure}"
                tvHumidity.text = "Humidity: ${item.humidity}"
                tvDescription.text = "Description: ${item.description}"
                tvSource.text = "Source: ${item.source}"
            }
        }
    }

    fun ViewGroup.inflateViewHolder(id: Int): View {
        return LayoutInflater.from(context).inflate(id, this, false)
    }
}