package uit.nguyenhung.nabweather.app.model.forecast

data class ForecastUI(
    val id: Long?,
    val date: String?,
    val avgTemperature: String?,
    val pressure: String?,
    val humidity: String?,
    val description: String?,
    val source: String = "none"
)