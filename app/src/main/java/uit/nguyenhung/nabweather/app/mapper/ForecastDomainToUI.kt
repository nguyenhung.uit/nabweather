package uit.nguyenhung.nabweather.app.mapper

import uit.nguyenhung.nabweather.app.model.forecast.ForecastUI
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.shared.ModelMapper
import uit.nguyenhung.nabweather.shared.format
import javax.inject.Inject

class ForecastDomainToUI @Inject constructor() : ModelMapper<Forecast, ForecastUI> {

    override fun map(from: Forecast) = ForecastUI(
        id = from.date?.time,
        date = from.date?.format(),
        avgTemperature = "${from.avgTemperature?.toInt()}${from.temperatureUnit.symbol}",
        pressure = "${from.pressure}",
        humidity = "${from.humidity}%",
        description = from.description,
    )
}