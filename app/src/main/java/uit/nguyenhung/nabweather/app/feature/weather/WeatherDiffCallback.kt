package uit.nguyenhung.nabweather.app.feature.weather

import androidx.recyclerview.widget.DiffUtil
import uit.nguyenhung.nabweather.app.model.forecast.ForecastUI

class WeatherDiffCallback(
    private val oldNormal: List<ForecastUI>,
    private val newNormal: List<ForecastUI>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldNormal.size

    override fun getNewListSize() = newNormal.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldNormal[oldItemPosition]
        val newItem = newNormal[newItemPosition]
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldNormal[oldItemPosition]
        val newItem = newNormal[newItemPosition]
        return oldItem == newItem
    }
}