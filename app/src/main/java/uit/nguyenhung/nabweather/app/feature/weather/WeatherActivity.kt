package uit.nguyenhung.nabweather.app.feature.weather

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import uit.nguyenhung.nabweather.R
import uit.nguyenhung.nabweather.databinding.ActivityWeatherBinding
import uit.nguyenhung.nabweather.shared.hideKeyboard

@AndroidEntryPoint
class WeatherActivity : AppCompatActivity() {

    private lateinit var viewModel: IWeatherViewModel

    private lateinit var adapter: WeatherAdapter

    private val binding: ActivityWeatherBinding by lazy {
        DataBindingUtil.setContentView(this, R.layout.activity_weather)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(
            this,
            defaultViewModelProviderFactory
        ).get(WeatherViewModel::class.java)

        binding.initView()
        viewModel.initViewModel()
    }

    private fun ActivityWeatherBinding.initView() {
        adapter = WeatherAdapter(rvWeather)

        btnGetWeather.setOnClickListener {
            edtQuery.hideKeyboard()
            viewModel.getForecasts(edtQuery.text.toString(), 17)
        }
    }

    private fun IWeatherViewModel.initViewModel() {
        forecastUI.observe(this@WeatherActivity, {
            binding.edtQuery.error = null
            adapter.submitList(it)
        })

        invalidSearchInput.observe(this@WeatherActivity, {
            it.getContentIfNotHandled()?.let { msg ->
                binding.edtQuery.error = msg
            }
        })

        commonError.observe(this@WeatherActivity, {
            it.getContentIfNotHandled()?.let { msg ->
                Toast.makeText(this@WeatherActivity, msg, Toast.LENGTH_SHORT).show()
            }
        })
    }
}