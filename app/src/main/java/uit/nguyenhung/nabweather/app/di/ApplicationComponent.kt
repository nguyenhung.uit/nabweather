package uit.nguyenhung.nabweather.app.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import uit.nguyenhung.nabweather.app.App
import uit.nguyenhung.nabweather.app.di.module.ApplicationModule
import uit.nguyenhung.nabweather.data.di.module.RepositoryModule
import uit.nguyenhung.nabweather.domain.di.module.UseCaseModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        RepositoryModule::class,
        UseCaseModule::class
        /*NetworkModule::class,*/
    ]
)
interface ApplicationComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun myApp(app: App): Builder

        fun build(): ApplicationComponent
    }
}