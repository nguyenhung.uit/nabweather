package uit.nguyenhung.nabweather.domain.usecase

const val INPUT_LENGTH_ERROR_MESSAGE = "the search input length must be from 3 characters or above"
const val NO_INTERNET_MESSAGE = "No internet connection, please retry again"

class CityQueryInvalidInput(msg: String = INPUT_LENGTH_ERROR_MESSAGE) : RuntimeException(msg)

class WeatherHttpException(msg: String = "") : RuntimeException(msg)

class NoConnectionException(msg: String = NO_INTERNET_MESSAGE) : RuntimeException(msg)