package uit.nguyenhung.nabweather.domain.repository

import io.reactivex.Single
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit

interface IWeatherRepository {

    fun getWeatherForecast(cityQuery: String, forecastDayNumbers: Int, units: TemperatureUnit)
            : Single<List<Forecast>>
}