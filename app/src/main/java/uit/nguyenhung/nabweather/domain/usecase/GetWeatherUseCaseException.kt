package uit.nguyenhung.nabweather.domain.usecase

import io.reactivex.Single
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import uit.nguyenhung.nabweather.domain.repository.IWeatherRepository
import javax.inject.Inject

interface IGetWeatherForecastUseCase {
    fun execute(
        cityQuery: String,
        forecastDayNumbers: Int,
        units: TemperatureUnit
    ): Single<List<Forecast>>
}

class GetWeatherForecastUseCase @Inject constructor(
    private val weatherRepository: IWeatherRepository
) : IGetWeatherForecastUseCase {

    override fun execute(
        cityQuery: String,
        forecastDayNumbers: Int,
        units: TemperatureUnit
    ): Single<List<Forecast>> {

        if (cityQuery.length < 3) {
            return Single.error(CityQueryInvalidInput())
        }

        return weatherRepository.getWeatherForecast(
            cityQuery,
            forecastDayNumbers,
            units
        )
    }
}