package uit.nguyenhung.nabweather.domain.model.forecast

enum class TemperatureUnit(
    val code: String,
    val symbol: String
) {
    CELSIUS("metric", "°C"),
    KELVIN("default", "°K"),
    FAHRENHEIT("imperial", "°F")
}