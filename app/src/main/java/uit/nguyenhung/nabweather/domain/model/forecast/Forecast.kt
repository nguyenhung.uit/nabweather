package uit.nguyenhung.nabweather.domain.model.forecast

import java.util.*

data class Forecast(
    val date: Date?,
    var temperatureUnit: TemperatureUnit = TemperatureUnit.CELSIUS,
    val avgTemperature: Double?,
    val pressure: Long?,
    val humidity: Long?,
    val description: String?,
)