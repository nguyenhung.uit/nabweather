package uit.nguyenhung.nabweather.domain.mapper

import uit.nguyenhung.nabweather.data.source.forecast.local.ForecastEntity
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.shared.ModelMapper
import java.util.*
import javax.inject.Inject

class ForecastEntityToDomain @Inject constructor() : ModelMapper<ForecastEntity, Forecast> {

    override fun map(from: ForecastEntity) = Forecast(
        date = Date(from.dateTime),
        avgTemperature = from.avgTemperature,
        pressure = from.pressure,
        humidity = from.humidity,
        description = from.description,
    )
}