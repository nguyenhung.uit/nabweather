package uit.nguyenhung.nabweather.domain.mapper

import uit.nguyenhung.nabweather.data.source.forecast.local.ForecastEntity
import uit.nguyenhung.nabweather.data.source.forecast.remote.ForecastResponse
import uit.nguyenhung.nabweather.data.source.forecast.remote.getAvgTemperature
import uit.nguyenhung.nabweather.shared.ModelMapper
import uit.nguyenhung.nabweather.shared.safe
import javax.inject.Inject

class ForecastResponseToEntity @Inject constructor() :
    ModelMapper<ForecastResponse, ForecastEntity> {

    override fun map(from: ForecastResponse) = ForecastEntity(
        dateTime = from.dateTime.safe() * 1000,
        avgTemperature = from.temperature.getAvgTemperature(),
        pressure = from.pressure,
        humidity = from.humidity,
        description = from.weathers?.map { it.description }
            ?.joinToString(separator = "-") { it.toString() }
    )
}