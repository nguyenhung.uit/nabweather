package uit.nguyenhung.nabweather.domain.di.module

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uit.nguyenhung.nabweather.domain.usecase.GetWeatherForecastUseCase
import uit.nguyenhung.nabweather.domain.usecase.IGetWeatherForecastUseCase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCaseModule {

    @Singleton
    @Binds
    internal abstract fun provideUserRepository(repository: GetWeatherForecastUseCase): IGetWeatherForecastUseCase
}