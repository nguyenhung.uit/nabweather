package uit.nguyenhung.nabweather.data.source.forecast.remote.interceptor

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import okhttp3.Interceptor
import okhttp3.Response
import uit.nguyenhung.nabweather.data.source.forecast.remote.HttpErrorResponse
import uit.nguyenhung.nabweather.domain.usecase.WeatherHttpException
import javax.inject.Inject

class HttpErrorHandlerInterceptor @Inject constructor(
    private val mGson: Gson
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        val responseBody = response.peekBody(Long.MAX_VALUE).string()

        if (response.code == 404) {
            try {
                val errorResponse = mGson.fromJson(responseBody, HttpErrorResponse::class.java)
                throw WeatherHttpException(errorResponse.message)
            } catch (e: JsonSyntaxException) {
                return response
            }
        }

        return response
    }
}