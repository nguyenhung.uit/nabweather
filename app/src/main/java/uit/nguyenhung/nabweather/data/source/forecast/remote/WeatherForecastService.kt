package uit.nguyenhung.nabweather.data.source.forecast.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import uit.nguyenhung.nabweather.BuildConfig

interface WeatherForecastService {

    @GET("forecast/daily")
    fun getWeatherForecast(
        @Query("appid") appId: String = BuildConfig.API_KEY,
        @Query("q") queryString: String,
        @Query("cnt") daysRange: Int,
        @Query("units") units: String
    ): Single<WeatherForecastResponse>
}