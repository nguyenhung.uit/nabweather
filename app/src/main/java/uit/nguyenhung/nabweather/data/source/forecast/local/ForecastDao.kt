package uit.nguyenhung.nabweather.data.source.forecast.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface ForecastDao {

    @Query(
        """
        SELECT * FROM $FORECAST_TABLE_NAME
        WHERE $FORECAST_CITY_QUERY = :cityQuery
        AND $FORECAST_DATE >= :todayAtMidnight
        AND $FORECAST_CODE_TEMPERATURE == :unitCode
        LIMIT :forecastDayNumbers
    """
    )
    fun getForecasts(
        cityQuery: String,
        forecastDayNumbers: Int,
        todayAtMidnight: Long,
        unitCode: String
    ): Single<List<ForecastEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertForecast(
        forecastEntities: List<ForecastEntity>
    )
}