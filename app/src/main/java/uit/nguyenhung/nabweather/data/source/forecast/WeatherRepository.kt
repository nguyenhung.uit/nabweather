package uit.nguyenhung.nabweather.data.source.forecast

import io.reactivex.Single
import uit.nguyenhung.nabweather.data.source.forecast.local.ForecastDao
import uit.nguyenhung.nabweather.data.source.forecast.remote.WeatherForecastService
import uit.nguyenhung.nabweather.domain.mapper.ForecastEntityToDomain
import uit.nguyenhung.nabweather.domain.mapper.ForecastResponseToDomain
import uit.nguyenhung.nabweather.domain.mapper.ForecastResponseToEntity
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import uit.nguyenhung.nabweather.domain.repository.IWeatherRepository
import uit.nguyenhung.nabweather.shared.atMidnight
import java.util.*
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private val forecastResponseToDomain: ForecastResponseToDomain,
    private val forecastResponseToEntity: ForecastResponseToEntity,
    private val forecastEntityToDomain: ForecastEntityToDomain,
    private val weatherForecastService: WeatherForecastService,
    private val forecastDao: ForecastDao,
) : IWeatherRepository {

    override fun getWeatherForecast(
        cityQuery: String,
        forecastDayNumbers: Int,
        units: TemperatureUnit
    ): Single<List<Forecast>> {
        var remoteThrowable: Throwable? = null

        val forecastLocalObs = forecastDao.getForecasts(
            cityQuery,
            forecastDayNumbers,
            Date().atMidnight(),
            units.code
        ).map {
            it.map { forecastItem ->
                forecastEntityToDomain.map(forecastItem).apply {
                    temperatureUnit = units
                }
            }
        }.flatMap {
            if (it.isEmpty()) {
                throw remoteThrowable!!
            } else {
                Single.just(it)
            }
        }

        val forecastRemoteObs = weatherForecastService.getWeatherForecast(
            queryString = cityQuery,
            daysRange = forecastDayNumbers,
            units = units.code
        ).doOnSuccess {
            // store into db
            it.forecastsResponse?.map { forecastItem ->
                forecastResponseToEntity.map(forecastItem).apply {
                    searchQuery = cityQuery
                    symbolTemperature = units.symbol
                    codeTemperature = units.code
                }
            }?.let { safeForecastEntities ->
                forecastDao.insertForecast(safeForecastEntities)
            }
        }.map {
            it.forecastsResponse?.map { forecastItem ->
                forecastResponseToDomain.map(forecastItem).apply {
                    temperatureUnit = units
                }
            } ?: emptyList()
        }

        return forecastRemoteObs.onErrorResumeNext {
            remoteThrowable = it
            forecastLocalObs
        }
    }
}