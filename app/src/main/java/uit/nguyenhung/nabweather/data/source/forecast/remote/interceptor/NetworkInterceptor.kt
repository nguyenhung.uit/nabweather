package uit.nguyenhung.nabweather.data.source.forecast.remote.interceptor

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import uit.nguyenhung.nabweather.domain.usecase.NoConnectionException
import uit.nguyenhung.nabweather.shared.NetworkUtil
import javax.inject.Inject

class NetworkInterceptor @Inject constructor(
    private val mContext: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (NetworkUtil.isNotConnected(mContext)) {
            throw NoConnectionException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}