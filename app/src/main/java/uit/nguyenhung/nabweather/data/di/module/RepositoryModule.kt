package uit.nguyenhung.nabweather.data.di.module

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uit.nguyenhung.nabweather.data.source.forecast.WeatherRepository
import uit.nguyenhung.nabweather.domain.repository.IWeatherRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    internal abstract fun provideUserRepository(repository: WeatherRepository): IWeatherRepository
}