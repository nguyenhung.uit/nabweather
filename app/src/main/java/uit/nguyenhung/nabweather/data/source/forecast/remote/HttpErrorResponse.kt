package uit.nguyenhung.nabweather.data.source.forecast.remote

import com.google.gson.annotations.SerializedName

data class HttpErrorResponse(
    @SerializedName("cod") val code: String,
    @SerializedName("message") val message: String
)