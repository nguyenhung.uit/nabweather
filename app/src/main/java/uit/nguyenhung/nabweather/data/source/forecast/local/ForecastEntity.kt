package uit.nguyenhung.nabweather.data.source.forecast.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index

const val FORECAST_TABLE_NAME = "FORECAST"
const val FORECAST_DATE = "date"
const val FORECAST_AVG_TEMPERATURE = "avg_temperature"
const val FORECAST_SYMBOL_TEMPERATURE = "symbol_temperature"
const val FORECAST_CODE_TEMPERATURE = "code_temperature"
const val FORECAST_PRESSURE = "pressure"
const val FORECAST_HUMIDITY = "humidity"
const val FORECAST_DES = "description"
const val FORECAST_CITY_QUERY = "city_query"

@Entity(
    primaryKeys = [FORECAST_CITY_QUERY, FORECAST_DATE, FORECAST_CODE_TEMPERATURE],
    tableName = FORECAST_TABLE_NAME,
    indices = [
        Index(value = [FORECAST_CITY_QUERY], unique = false),
        Index(value = [FORECAST_DATE], unique = false),
        Index(value = [FORECAST_CODE_TEMPERATURE], unique = false)
    ]
)
data class ForecastEntity(

    @ColumnInfo(name = FORECAST_CITY_QUERY) var searchQuery: String = "",
    @ColumnInfo(name = FORECAST_DATE) val dateTime: Long = 0,
    @ColumnInfo(name = FORECAST_AVG_TEMPERATURE) val avgTemperature: Double? = null,
    @ColumnInfo(name = FORECAST_SYMBOL_TEMPERATURE) var symbolTemperature: String = "",
    @ColumnInfo(name = FORECAST_CODE_TEMPERATURE) var codeTemperature: String = "",
    @ColumnInfo(name = FORECAST_PRESSURE) val pressure: Long?,
    @ColumnInfo(name = FORECAST_HUMIDITY) val humidity: Long?,
    @ColumnInfo(name = FORECAST_DES) val description: String?,
)