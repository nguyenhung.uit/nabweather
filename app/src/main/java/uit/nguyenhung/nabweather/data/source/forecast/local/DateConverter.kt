package uit.nguyenhung.nabweather.data.source.forecast.local

import androidx.room.TypeConverter
import java.text.ParseException
import java.util.*

class DateConverter {

    @TypeConverter
    fun toDate(value: Long?): Date? {
        return try {
            value?.let {
                Date(it)
            }
        } catch (e: ParseException) {
            null
        }
    }

    @TypeConverter
    fun toLong(value: Date?): Long? {
        return value?.time ?: 0
    }
}