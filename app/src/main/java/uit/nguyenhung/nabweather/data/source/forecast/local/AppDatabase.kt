package uit.nguyenhung.nabweather.data.source.forecast.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(
    entities = [
        ForecastEntity::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val APP_DATABASE_NAME = "NAB_DB"
    }

    abstract fun createForecastDao(): ForecastDao
}