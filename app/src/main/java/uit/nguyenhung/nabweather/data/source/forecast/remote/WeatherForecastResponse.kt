package uit.nguyenhung.nabweather.data.source.forecast.remote

import com.google.gson.annotations.SerializedName
import uit.nguyenhung.nabweather.shared.safe

data class WeatherForecastResponse(
    @SerializedName("list") val forecastsResponse: List<ForecastResponse>?
)

data class ForecastResponse(
    @SerializedName("dt") val dateTime: Long?,
    @SerializedName("temp") val temperature: TemperatureResponse,
    @SerializedName("pressure") val pressure: Long?,
    @SerializedName("humidity") val humidity: Long?,
    @SerializedName("weather") val weathers: List<WeatherResponse>?
)

data class TemperatureResponse(
    @SerializedName("min") val min: Double?,
    @SerializedName("max") val max: Double?,
)

fun TemperatureResponse.getAvgTemperature() = (min.safe() + max.safe()) / 2

data class WeatherResponse(
    @SerializedName("description") val description: String?
)