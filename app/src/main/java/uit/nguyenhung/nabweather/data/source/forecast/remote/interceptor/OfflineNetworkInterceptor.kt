package uit.nguyenhung.nabweather.data.source.forecast.remote.interceptor

import android.content.Context
import android.util.Log
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import uit.nguyenhung.nabweather.data.di.module.HEADER_CACHE_CONTROL
import uit.nguyenhung.nabweather.data.di.module.HEADER_PRAGMA
import uit.nguyenhung.nabweather.shared.NetworkUtil
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class OfflineNetworkInterceptor @Inject constructor(
    private val context: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if (NetworkUtil.isNotConnected(context)) {
            val cacheControl = CacheControl.Builder()
                .maxStale(7, TimeUnit.DAYS)
                .build()
            request = request.newBuilder()
                .removeHeader(HEADER_PRAGMA)
                .removeHeader(HEADER_CACHE_CONTROL)
                .cacheControl(cacheControl)
                .build()
        }

        return chain.proceed(request)
    }
}