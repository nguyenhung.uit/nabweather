package uit.nguyenhung.nabweather.activity

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matchers.not
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uit.nguyenhung.RecyclerViewTestingAction.atPosition
import uit.nguyenhung.nabweather.R
import uit.nguyenhung.nabweather.app.feature.weather.WeatherActivity
import uit.nguyenhung.nabweather.domain.usecase.INPUT_LENGTH_ERROR_MESSAGE
import uit.nguyenhung.nabweather.shared.format
import java.util.*

@LargeTest
class WeatherActivityTest {

    @Rule
    @JvmField
    var weatherActivityTestRule: ActivityScenarioRule<WeatherActivity> =
        ActivityScenarioRule(WeatherActivity::class.java)

    private var cityQueryValid = "hanoi"
    private var cityQueryValid2 = "hanoiii"
    private var cityQueryInValid = "ha"

    private var date = "Date: ${Date().format()}"

    @Before
    fun prepare() {

    }

    @Test
    fun input_valid_cityquery_expect_the_date_of_first_item_show_correct() {
        // arrange
        onView(withId(R.id.edtQuery))
            .perform(ViewActions.clearText())
            .perform(ViewActions.typeText(cityQueryValid))

        onView(withId(R.id.edtQuery))
            .perform(ViewActions.closeSoftKeyboard())

        onView(withId(R.id.btnGetWeather))
            .perform(ViewActions.click())

        Thread.sleep(3000)

        // assert
        onView(withId(R.id.rvWeather))
            .check(matches(atPosition(0, hasDescendant(withText(date)))))
    }

    @Test
    fun input_invalid_cityqueyry_expect_show_error_text() {
        // arrange
        onView(withId(R.id.edtQuery))
            .perform(ViewActions.clearText())
            .perform(ViewActions.typeText(cityQueryInValid))

        onView(withId(R.id.edtQuery))
            .perform(ViewActions.closeSoftKeyboard())

        onView(withId(R.id.btnGetWeather))
            .perform(ViewActions.click())

        Thread.sleep(3000)

        // assert
        onView(withId(R.id.edtQuery))
            .check(matches(hasErrorText(INPUT_LENGTH_ERROR_MESSAGE)))
    }

    @Test
    fun input_valid_cityqueyry_but_wrong_city_name_expect_show_error_by_toast() {
        // arrange
        var activity: WeatherActivity? = null
        weatherActivityTestRule.scenario.onActivity { a ->
            activity = a
        }

        Thread.sleep(500)

        onView(withId(R.id.edtQuery))
            .perform(ViewActions.clearText())
            .perform(ViewActions.typeText(cityQueryValid2))

        onView(withId(R.id.edtQuery))
            .perform(ViewActions.closeSoftKeyboard())

        onView(withId(R.id.btnGetWeather))
            .perform(ViewActions.click())

        Thread.sleep(500)

        // assert
        onView(withText("city not found"))
            .inRoot(withDecorView(not(activity?.window?.decorView)))
            .check(matches(isDisplayed()))
    }
}