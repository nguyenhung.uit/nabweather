package uit.nguyenhung.nabweather.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.internal.verification.Times
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import uit.nguyenhung.nabweather.app.Event
import uit.nguyenhung.nabweather.app.feature.weather.IWeatherViewModel
import uit.nguyenhung.nabweather.app.feature.weather.WeatherViewModel
import uit.nguyenhung.nabweather.app.mapper.ForecastDomainToUI
import uit.nguyenhung.nabweather.app.model.forecast.ForecastUI
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import uit.nguyenhung.nabweather.domain.usecase.CityQueryInvalidInput
import uit.nguyenhung.nabweather.domain.usecase.IGetWeatherForecastUseCase
import uit.nguyenhung.nabweather.domain.usecase.NoConnectionException
import uit.nguyenhung.nabweather.domain.usecase.WeatherHttpException
import uit.nguyenhung.nabweather.rule.RxImmediateSchedulerRule
import java.util.*

@RunWith(PowerMockRunner::class)
@PrepareForTest(
    value = [
        IGetWeatherForecastUseCase::class,
        ForecastDomainToUI::class
    ]
)
@PowerMockIgnore("javax.management.*")
class WeatherViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    private lateinit var getWeatherForecastUseCase: IGetWeatherForecastUseCase

    //@Mock
    private var forecastDomainToUI = ForecastDomainToUI()


    private lateinit var viewModel: IWeatherViewModel

    private var cityQueryValid = "hanoi"
    private var cityQueryInValid = "ha"
    private var forecastDayNumber = 17

    private var forecast1 = Forecast(
        date = Date(1610946000000),
        temperatureUnit = TemperatureUnit.CELSIUS,
        avgTemperature = 15.594999999999999,
        pressure = 1024,
        humidity = 35,
        description = "scattered clouds"
    )

    private var forecast2 = Forecast(
        date = Date(1611032400000),
        temperatureUnit = TemperatureUnit.CELSIUS,
        avgTemperature = 17.669999999999998,
        pressure = 1019,
        humidity = 36,
        description = "sky is clear"
    )

    private var forecastUI1 = ForecastUI(
        id = 1610946000000,
        date = "Mon, 18 Jan 2021",
        avgTemperature = "15°C",
        pressure = "1024",
        humidity = "35%",
        description = "scattered clouds"
    )

    private var forecastUI2 = ForecastUI(
        id = 1611032400000,
        date = "Tue, 19 Jan 2021",
        avgTemperature = "17°C",
        pressure = "1019",
        humidity = "36%",
        description = "sky is clear"
    )

    private var forecastUIs = listOf(forecastUI1, forecastUI2)
    private var forecasts = listOf(forecast1, forecast2)

    private val invalidSearchInputObserver: Observer<Event<String>> = mock()
    private val commonErrorObserver: Observer<Event<String>> = mock()
    private val forecastUIObserver: Observer<List<ForecastUI>> = mock()

    @Before
    fun prepareTest() {
        viewModel = WeatherViewModel(
            getWeatherForecastUseCase,
            forecastDomainToUI
        )
    }

    @Test
    fun `enter invalid city query, expect call forecastUI livedata with non empty list `() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        whenever(
            getWeatherForecastUseCase.execute(
                cityQueryValid,
                forecastDayNumber,
                temperature
            )
        ).thenReturn(Single.just(forecasts))

        viewModel.forecastUI.observeForever(forecastUIObserver)

        // action
        viewModel.getForecasts(
            cityQueryValid,
            forecastDayNumber,
            temperature
        )

        // assert
        verify(forecastUIObserver, Times(1)).onChanged(forecastUIs)
    }

    @Test
    fun `enter invalid city query, expect call invalid input livedata & forecastUI live is not called`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        val invalidMsgError = "invalid msg error test"
        whenever(
            getWeatherForecastUseCase.execute(
                cityQueryInValid,
                forecastDayNumber,
                temperature
            )
        ).thenReturn(Single.error(CityQueryInvalidInput(invalidMsgError)))

        viewModel.invalidSearchInput.observeForever(invalidSearchInputObserver)
        viewModel.forecastUI.observeForever(forecastUIObserver)

        // action
        viewModel.getForecasts(
            cityQueryInValid,
            forecastDayNumber,
            temperature
        )

        // assert
        verify(invalidSearchInputObserver, Times(1)).onChanged(any())
        verify(forecastUIObserver, never()).onChanged(forecastUIs)
    }

    @Test
    fun `enter valid city query, turn off internet, expect call forecastUI && commonError livedata`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        val noInternetMsgError = "no internet msg error test"
        whenever(
            getWeatherForecastUseCase.execute(
                cityQueryValid,
                forecastDayNumber,
                temperature
            )
        ).thenReturn(Single.error(NoConnectionException(noInternetMsgError)))

        viewModel.commonError.observeForever(commonErrorObserver)
        viewModel.forecastUI.observeForever(forecastUIObserver)

        // action
        viewModel.getForecasts(
            cityQueryValid,
            forecastDayNumber,
            temperature
        )

        // assert
        verify(commonErrorObserver, Times(1)).onChanged(any())
        verify(forecastUIObserver, Times(1)).onChanged(emptyList())
    }

    @Test
    fun `enter valid city query, throw http exception (city not found), expect call forecastUI && commonError livedata`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        val cityNotFoundMsgError = "city not found msg error test"
        whenever(
            getWeatherForecastUseCase.execute(
                cityQueryValid,
                forecastDayNumber,
                temperature
            )
        ).thenReturn(Single.error(WeatherHttpException(cityNotFoundMsgError)))

        viewModel.commonError.observeForever(commonErrorObserver)
        viewModel.forecastUI.observeForever(forecastUIObserver)

        // action
        viewModel.getForecasts(
            cityQueryValid,
            forecastDayNumber,
            temperature
        )

        // assert
        verify(commonErrorObserver, Times(1)).onChanged(any())
        verify(forecastUIObserver, Times(1)).onChanged(emptyList())
    }
}