package uit.nguyenhung.nabweather.mapper

import org.junit.Test
import uit.nguyenhung.nabweather.app.mapper.ForecastDomainToUI
import uit.nguyenhung.nabweather.app.model.forecast.ForecastUI
import uit.nguyenhung.nabweather.data.source.forecast.remote.ForecastResponse
import uit.nguyenhung.nabweather.data.source.forecast.remote.TemperatureResponse
import uit.nguyenhung.nabweather.data.source.forecast.remote.WeatherResponse
import uit.nguyenhung.nabweather.domain.mapper.ForecastEntityToDomain
import uit.nguyenhung.nabweather.domain.mapper.ForecastResponseToDomain
import uit.nguyenhung.nabweather.domain.mapper.ForecastResponseToEntity
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import java.util.*

class ModelMapperTest {

    private val forecastEntityToDomain = ForecastEntityToDomain()
    private val forecastResponseToDomain = ForecastResponseToDomain()
    private val forecastResponseToEntity = ForecastResponseToEntity()
    private val forecastDomainToUI = ForecastDomainToUI()

    private var forecast = Forecast(
        date = Date(1610946000000),
        temperatureUnit = TemperatureUnit.CELSIUS,
        avgTemperature = 15.594999999999999,
        pressure = 1024,
        humidity = 35,
        description = "scattered clouds"
    )

    private var forecastUI = ForecastUI(
        id = 1610946000000,
        date = "Mon, 18 Jan 2021",
        avgTemperature = "15°C",
        pressure = "1024",
        humidity = "35%",
        description = "scattered clouds"
    )

    private var forecastResponse = ForecastResponse(
        dateTime = 1610946000,
        temperature = TemperatureResponse(
            min = 11.08,
            max = 20.11
        ),
        pressure = 1024,
        humidity = 35,
        weathers = listOf(
            WeatherResponse(
                description = "scattered clouds"
            )
        )
    )

    @Test
    fun `test forecastDomainToUI`() {
        // arrange

        //action
        val forecastUIOutput = forecastDomainToUI.map(forecast)

        // assert
        assert(forecastUIOutput == forecastUI)
    }

    @Test
    fun `test forecastResponseToDomain`() {
        // arrange

        //action
        val forecastOutput = forecastResponseToDomain.map(forecastResponse)

        // assert
        assert(forecastOutput == forecast)
    }
}