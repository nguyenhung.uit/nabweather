package uit.nguyenhung.nabweather.repository

import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import uit.nguyenhung.nabweather.BuildConfig
import uit.nguyenhung.nabweather.data.source.forecast.WeatherRepository
import uit.nguyenhung.nabweather.data.source.forecast.local.ForecastDao
import uit.nguyenhung.nabweather.data.source.forecast.local.ForecastEntity
import uit.nguyenhung.nabweather.data.source.forecast.remote.*
import uit.nguyenhung.nabweather.domain.mapper.ForecastEntityToDomain
import uit.nguyenhung.nabweather.domain.mapper.ForecastResponseToDomain
import uit.nguyenhung.nabweather.domain.mapper.ForecastResponseToEntity
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import uit.nguyenhung.nabweather.domain.repository.IWeatherRepository
import uit.nguyenhung.nabweather.domain.usecase.WeatherHttpException
import uit.nguyenhung.nabweather.shared.atMidnight
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class WeatherRepositoryTest {

    private lateinit var weatherRepository: IWeatherRepository

    private val forecastResponseToDomain = ForecastResponseToDomain()
    private val forecastResponseToEntity = ForecastResponseToEntity()
    private val forecastEntityToDomain = ForecastEntityToDomain()

    @Mock
    private lateinit var weatherForecastService: WeatherForecastService

    @Mock
    private lateinit var forecastDao: ForecastDao

    private var cityQueryValid = "hanoi"
    private var forecastDayNumber = 17

    private var forecastResponse = ForecastResponse(
        dateTime = 1610946000,
        temperature = TemperatureResponse(
            min = 11.08,
            max = 20.11
        ),
        pressure = 1024,
        humidity = 35,
        weathers = listOf(
            WeatherResponse(
                description = "scattered clouds"
            )
        )
    )

    private var forecastEntity = ForecastEntity(
        searchQuery = cityQueryValid,
        dateTime = 1610946000000,
        avgTemperature = 15.594999999999999,
        symbolTemperature = TemperatureUnit.CELSIUS.symbol,
        pressure = 1024,
        humidity = 35,
        description = "scattered clouds"
    )

    private var weatherForecastResponse = WeatherForecastResponse(listOf(forecastResponse))

    @Before
    fun prepareTest() {
        weatherRepository = WeatherRepository(
            forecastResponseToDomain,
            forecastResponseToEntity,
            forecastEntityToDomain,
            weatherForecastService,
            forecastDao
        )
    }

    @Test
    fun `call api from network successfully, expect forecast result not null and not empty`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        whenever(
            weatherForecastService.getWeatherForecast(
                BuildConfig.API_KEY,
                cityQueryValid,
                forecastDayNumber,
                temperature.code
            )
        ).thenReturn(Single.just(weatherForecastResponse))

        whenever(
            forecastDao.getForecasts(
                cityQueryValid,
                forecastDayNumber,
                Date().atMidnight(),
                temperature.code
            )
        ).thenReturn(Single.just(listOf(forecastEntity)))

        // action
        val testObservable = weatherRepository.getWeatherForecast(
            cityQueryValid,
            forecastDayNumber,
            temperature
        ).test()

        // assert
        testObservable.assertComplete()
        testObservable.assertValue {
            it.isNotEmpty() && it.size == 1
        }
    }

    @Test
    fun `call api from network fail & from local successfully, expect forecast result not null and not empty`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        whenever(
            weatherForecastService.getWeatherForecast(
                BuildConfig.API_KEY,
                cityQueryValid,
                forecastDayNumber,
                temperature.code
            )
        ).thenReturn(Single.error(WeatherHttpException()))

        whenever(
            forecastDao.getForecasts(
                cityQueryValid,
                forecastDayNumber,
                Date().atMidnight(),
                temperature.code
            )
        ).thenReturn(Single.just(listOf(forecastEntity)))

        // action
        val testObservable = weatherRepository.getWeatherForecast(
            cityQueryValid,
            forecastDayNumber,
            temperature
        ).test()

        // assert
        testObservable.assertComplete()
        testObservable.assertValue {
            it.isNotEmpty() && it.size == 1
        }
    }

    @Test
    fun `call api from network fail & from local got empty result, expect emit error`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        whenever(
            weatherForecastService.getWeatherForecast(
                BuildConfig.API_KEY,
                cityQueryValid,
                forecastDayNumber,
                temperature.code
            )
        ).thenReturn(Single.error(WeatherHttpException()))

        whenever(
            forecastDao.getForecasts(
                cityQueryValid,
                forecastDayNumber,
                Date().atMidnight(),
                temperature.code
            )
        ).thenReturn(Single.just(emptyList()))

        // action
        val testObservable = weatherRepository.getWeatherForecast(
            cityQueryValid,
            forecastDayNumber,
            temperature
        ).test()

        // assert
        testObservable.assertError {
            it is WeatherHttpException
        }
    }
}