package uit.nguyenhung.nabweather.usecase

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import uit.nguyenhung.nabweather.domain.model.forecast.Forecast
import uit.nguyenhung.nabweather.domain.model.forecast.TemperatureUnit
import uit.nguyenhung.nabweather.domain.repository.IWeatherRepository
import uit.nguyenhung.nabweather.domain.usecase.CityQueryInvalidInput
import uit.nguyenhung.nabweather.domain.usecase.GetWeatherForecastUseCase
import uit.nguyenhung.nabweather.domain.usecase.IGetWeatherForecastUseCase
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class GetWeatherForecastUseCaseTest {

    @Mock
    private lateinit var weatherRepository: IWeatherRepository

    private lateinit var useCaseTest: IGetWeatherForecastUseCase

    private var forecast1 = Forecast(
        date = Date(1610946000000),
        temperatureUnit = TemperatureUnit.CELSIUS,
        avgTemperature = 15.594999999999999,
        pressure = 1024,
        humidity = 35,
        description = "scattered clouds"
    )

    private var forecast2 = Forecast(
        date = Date(1611032400000),
        temperatureUnit = TemperatureUnit.CELSIUS,
        avgTemperature = 17.669999999999998,
        pressure = 1019,
        humidity = 36,
        description = "sky is clear"
    )

    private var forecasts = listOf(forecast1, forecast2)

    private var cityQueryValid = "hanoi"
    private var cityQueryInValid = "ha"
    private var forecastDayNumber = 17

    @Before
    fun prepareTest() {
        useCaseTest = GetWeatherForecastUseCase(weatherRepository)
    }

    @Test
    fun `input valid cityQuery, expect receive a correct list of forecast`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS
        whenever(
            weatherRepository.getWeatherForecast(
                cityQueryValid,
                forecastDayNumber,
                temperature
            )
        ).thenReturn(Single.just(forecasts))

        // action
        val testObserver = useCaseTest.execute(
            cityQueryValid,
            forecastDayNumber,
            temperature
        ).test()

        // assert
        testObserver.assertComplete()
        testObserver.assertValue {
            it == forecasts
        }
    }

    @Test
    fun `assign length of cityQuery less than 3, expect weatherRepository does not call sand throw error of CityQueryInvalidInput`() {
        // arrange
        val temperature = TemperatureUnit.CELSIUS

        // action
        val testObserver = useCaseTest.execute(
            cityQueryInValid,
            forecastDayNumber,
            temperature
        ).test()

        // assert
        testObserver.assertError {
            it is CityQueryInvalidInput
        }
        verify(weatherRepository, never()).getWeatherForecast(any(), any(), any())
    }
}